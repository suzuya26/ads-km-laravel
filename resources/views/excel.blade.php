@extends('layouts.app')
@section('content')
<div class="text-center ">
    <h1>IMPORT & EXPORT EXCELL</h1>
</div>
<div class="row">
    <div class="d-flex justify-content-center p-5">
        <div class="col-md-10">

            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                        <strong>{{ $message }}</strong>
                </div>
            @endif

            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                        <strong>{{ $message }}</strong>
                </div>
            @endif

            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="d-flex justify-content-center pb-4">
                                    <div class="h4">Import User</div>
                                </div>
                                <label>Pilh File</label>
                                <div class="custom-file">
                                    <input required type="file" class="form-control" name="file" id="customFile">
                                </div>
                                <label style="font-size: 10px">Tipe file : csv,xls,xlsx</label>
                                <div class="d-flex justify-content-center pb-5">
                                    <button id="tarif-import" type="submit" class="btn btn-primary" formaction="{{ route('import.user') }}">Import User</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="d-flex justify-content-center pb-4">
                                <div class="h4">Export User</div>
                            </div>
                            <div class="d-flex justify-content-center pb-5">
                                <a class="btn btn-primary" href="{{ route('export.user') }}">Export User</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
