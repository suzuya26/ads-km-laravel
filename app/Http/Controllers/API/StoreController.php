<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\StoreResource;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    //untuk toko user yg sedang login
    public function show($id){
        $store = auth()->user()->store()->find($id);
        if (!$store) {
            return response()->json([
                'message'   => 'error',
                'data'      => 'Store not Found',
            ]);
        }

        return response()->json([
            'message'   => 'success',
            'data'      => new StoreResource($store),
        ]);
    }
    public function store(Request $request)
    {
        // $store = auth()->user()->store()->find(1);
        // if($store){
        //     return response()->json([
        //         'message' => "kamu sudah punya toko"
        //     ]);
        // }
        //sementara pakai error dg constraint unique
        $store= auth()->user()->store()->create([
            'name' => $request->name,
        ]);

        return response()->json([
            'message' => 'success',
            'data' => new StoreResource($store),
        ]);
    }
}
