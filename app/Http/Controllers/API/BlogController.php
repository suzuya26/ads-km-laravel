<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\BlogResource;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    public function index()
    {
        $blog = auth()->user()->blogs()
            ->where('title', 'like', '%' . request('keyword') . '%')
            ->paginate(10);

        return response()->json([
            'message'   => 'success',
            'data'      => BlogResource::collection($blog),
        ]);
    }
    public function store(Request $request)
    {
        $cover      = $request->file('cover');
        $fileName   = time() . '_' . $cover->getClientOriginalName();
        $filePath   = $cover->storeAs('images/articles', $fileName, 'public');

        $blog = auth()->user()->blogs()->create([
            'cover'     => $filePath,
            'title'     => $request->title,
            'slug'      => Str::slug($request->title),
            'content'   => $request->content,
        ]);

        return response()->json([
            'message'   => 'success',
            'data'      => new BlogResource($blog),
        ]);
    }
    public function show($id)
    {
        $blog = auth()->user()->blogs()->find($id);

        if (!$blog) {
            return response()->json([
                'message'   => 'error',
                'data'      => 'Blog not found',
            ]);
        }

        return response()->json([
            'message'   => 'success',
            'data'      => new BlogResource($blog),
        ]);
    }
    public function update(Request $request, $id)
    {
        $blog = auth()->user()->blogs()->find($id);

        if (!$blog) {
            return response()->json([
                'message'   => 'error',
                'data'      => 'Blog not found',
            ]);
        }

        $cover      = $request->file('cover');
        if ($cover) {
            Storage::delete('public/' . $blog->cover);
            $fileName   = time() . '_' . $cover->getClientOriginalName();
            $filePath   = $cover->storeAs('images/articles', $fileName, 'public');
        } else {
            $filePath   = $blog->cover;
        }


        $blog->update([
            'cover'     => $filePath,
            'title'     => $request->title ?? $blog->title,
            'slug'      => $request->title ? Str::slug($request->title) : $blog->slug,
            'content'   => $request->content ?? $blog->content,
        ]);

        return response()->json([
            'message'   => 'success',
            'data'      => new BlogResource($blog),
        ]);
    }
    public function destroy($id)
    {
        $blog = auth()->user()->blogs()->find($id);

        if (!$blog) {
            return response()->json([
                'message'   => 'error',
                'data'      => 'Blog not found',
            ]);
        }

        Storage::delete('public/' . $blog->cover);
        $blog->delete();

        return response()->json([
            'message'   => 'success',
        ]);
    }
}
